package service

import (
	"strings"

	"gitlab.com/lodg/go-util/cursor"
	"gitlab.com/lodg/go-util/fn"

	"github.com/VividCortex/mysqlerr"
	"github.com/asaskevich/govalidator"
	"github.com/go-sql-driver/mysql"
	"github.com/gogo/protobuf/proto"
	"github.com/grpc-ecosystem/grpc-gateway/runtime"
	"gitlab.com/lodg/go-util/auth"
	"gitlab.com/lodg/go-util/logging"

	"gitlab.com/lodg/go-util/svc"
	"gitlab.com/lodg/svc-properties/repo"
	"gitlab.com/lodg/svc-properties/repo/model"
	"gitlab.com/lodg/svc-properties/rpc"
	"gitlab.com/lodg/svc-properties/rpc/mc"
	"golang.org/x/net/context"
	"google.golang.org/genproto/googleapis/rpc/code"
	"google.golang.org/genproto/googleapis/rpc/errdetails"
	"google.golang.org/grpc"
)

// codes.Unimplemented
const (
	{{baseTypeLower}}Service         = "service.{{baseTypeLower}}"
	property{{BaseType}}Service = "service.property.{{baseTypeLower}}"
)

// {{BaseType}} service
type {{BaseType}} struct {
	*svc.Service
}

func init() {
	// {{baseTypeLower}}
	Registry.AddServer({{baseTypeLower}}Service, func(server *grpc.Server) (err error) {
		rpc.Register{{BaseType}}ServiceServer(server, New{{BaseType}}())
		return
	})

	Registry.AddGateway({{baseTypeLower}}Service, func(ctx context.Context, mux *runtime.ServeMux, endpoint string, opts []grpc.DialOption) error {
		return rpc.Register{{BaseType}}ServiceHandlerFromEndpoint(ctx, mux, endpoint, opts)
	})

}

// New{{BaseType}} creates a new service
func New{{BaseType}}() *{{BaseType}} {
	return &{{BaseType}}{&svc.Service{
		Logger: logging.New({{baseTypeLower}}Service),
	}}
}

// Get -- GET request for a {{baseTypeLower}}. Returns a {{BaseType}} object as defined in {{baseTypeLower}}.proto
func ({{FirstLetter}} *{{BaseType}}) Get(ctx context.Context, req *rpc.{{BaseType}}GetRequest) (*rpc.{{BaseType}}GetResponse, error) {
	resp := &rpc.{{BaseType}}GetResponse{
		Success: false,
	}
	{{baseTypeLower}}Repo := repo.New{{BaseType}}(ctx)
	found{{BaseType}}, err := repo.New{{BaseType}}(ctx).Get({{baseTypeLower}}Repo.MakeLookup(req.Id, auth.GetUserIDFromContext(ctx), req.PropertyId))
	if err != nil || req.Id == "" { // the only possible error here is that the lookup fails
		errors := {{FirstLetter}}.Errors(ctx, code.Code_INTERNAL)
		errors.
			SetCode(code.Code_NOT_FOUND).
			AddGenericErrorCode(mc.Code_ERR_TRANSACTION_NOT_FOUND)
		resp.Error = errors.Build()

		return resp, svc.RPCError(resp.Error.Code, resp)
	}
	resp.Success = true
	resp.Data = {{FirstLetter}}.ToRPC(found{{BaseType}})

	return resp, err
}

func ({{FirstLetter}} *{{BaseType}}) makeFilter(ctx context.Context, req *rpc.{{BaseType}}ListRequest) *model.{{BaseType}}Filter {
	// make the call to repo here
	filter := &model.{{BaseType}}Filter{
		OwnerID: auth.GetUserIDFromContext(ctx),
	}

	if req.PropertyId != "" {
		filter.PropertyIDs = fn.StrMap(strings.Split(req.PropertyId, ","), func(s string) string {
			return strings.TrimSpace(s)
		})
	}

	if req.Type != rpc.{{BaseType}}_UNKNOWN {
		filter.Type = req.Type.String()
	}

	if req.From != nil {
		filter.From = fn.ToTime(req.From)
	}
	if req.To != nil {
		filter.To = fn.ToTime(req.To)
	}
	return filter
}

// List -- (GET) request to get all {{FirstLetter}}erties owned by the owner of the request token. Returns a list of {{BaseType}} objects
func ({{FirstLetter}} *{{BaseType}}) List(ctx context.Context, req *rpc.{{BaseType}}ListRequest) (*rpc.{{BaseType}}ListResponse, error) {
	resp := &rpc.{{BaseType}}ListResponse{
		Success: false,
	}
	{{baseTypeLower}}Repo := repo.New{{BaseType}}(ctx)

	// validate pagination
	limit, errors := cursor.ValidatePaginatedDefault(ctx, req)
	if errors != nil {
		resp.Error = errors.Build()
		return resp, svc.RPCError(resp.Error.Code, resp)
	}

	// make the call to repo here
	col, err := {{baseTypeLower}}Repo.List({{FirstLetter}}.makeFilter(ctx, req), req.Cursor, limit, 0)

	if err != nil {
		errors := {{FirstLetter}}.Errors(ctx, code.Code_INTERNAL)
		errors.
			SetCode(code.Code_INTERNAL).
			AddGenericErrorCode(mc.Code_ERR_INTERNAL) // this should never happen
		resp.Error = errors.Build()
		{{FirstLetter}}.Logger.Errorf("Unknown error propagated: %s", err)
		return resp, svc.RPCError(resp.Error.Code, resp)
	}

	// convert array of model.{{BaseType}} to *rpc.{{BaseType}} (note that model.{{baseTypeLower}} is not a pointer so we pass the address)
	var rpc{{BaseType}} []*rpc.{{BaseType}}
	for _, {{baseTypeLower}} := range col.GetItems() {
		converted{{BaseType}} := {{FirstLetter}}.ToRPC({{baseTypeLower}})

		rpc{{BaseType}} = append(rpc{{BaseType}}, converted{{BaseType}})

	}
	resp.Data = &rpc.{{BaseType}}List{
		Count:          int64(len(rpc{{BaseType}})),
		Items:          rpc{{BaseType}},
		NextCursor:     col.NextCursor().GenBase64(),
		PreviousCursor: col.PrevCursor().GenBase64(),
	}
	resp.Success = true
	return resp, nil
}

// Create -- (POST) request to make a new {{baseTypeLower}}. Returns a {{BaseType}} object
func ({{FirstLetter}} *{{BaseType}}) Create(ctx context.Context, req *rpc.{{BaseType}}CreateRequest) (*rpc.{{BaseType}}CreateResponse, error) {
	resp := &rpc.{{BaseType}}CreateResponse{
		Success: false,
	}
	//don't use req before validateMessage is called or you risk SEGFAULTs happening
	if errors := {{FirstLetter}}.validateMessage(ctx, req); errors != nil {
		resp.Error = errors.Build()
		return resp, svc.RPCError(resp.Error.Code, resp)
	}
	if !{{FirstLetter}}.verifyOwner(ctx, req.{{BaseType}}.PropertyId) {
		errors := {{FirstLetter}}.Errors(ctx, code.Code_INTERNAL)
		errors.
			SetCode(code.Code_NOT_FOUND).
			AddGenericErrorCode(mc.Code_ERR_NO_PERMISSIONS)
		resp.Error = errors.Build()
		return resp, svc.RPCError(resp.Error.Code, resp)
	}
	if req.{{BaseType}}.IsRecurring != false {
		errors := {{FirstLetter}}.Errors(ctx, code.Code_INVALID_ARGUMENT)
		errors. // we do not allow recurring {{baseTypeLower}}s to be set directly through the service
			SetCode(code.Code_NOT_FOUND).
			AddBadRequestFieldViolations(mc.Code_ERR_NO_PERMISSIONS, &errdetails.BadRequest_FieldViolation{
				Field:       "is_recurring",
				Description: TF(ctx)(mc.Code_ERR_NO_PERMISSIONS.String()),
			})
		resp.Error = errors.Build()
		return resp, svc.RPCError(resp.Error.Code, resp)
	}
	{{baseTypeLower}}Repo := repo.New{{BaseType}}(ctx)
	new{{BaseType}} := {{FirstLetter}}.ToModel(req.{{BaseType}}, "", auth.GetUserIDFromContext(ctx))

	// make the repo call here
	{{baseTypeLower}}, err := {{baseTypeLower}}Repo.Create(new{{BaseType}})
	if err != nil {
		errors := {{FirstLetter}}.Errors(ctx, code.Code_INTERNAL)
		switch e := err.(type) {
		case govalidator.Errors:
			var inconsistent{{BaseType}} bool
			errors.SetCode(code.Code_INVALID_ARGUMENT)
			violations := []*errdetails.BadRequest_FieldViolation{}
			for k := range govalidator.ErrorsByField(err) {
				if k == repo.ErrInconsistent{{BaseType}} {
					inconsistent{{BaseType}} = true
					continue
				}
				violations = append(violations, &errdetails.BadRequest_FieldViolation{
					Field:       k,
					Description: TF(ctx)(mc.Code_ERR_INPUT_FAILS_PRECONDITION.String()),
				})
			}
			if len(violations) > 0 {
				errors.AddBadRequestFieldViolations(mc.Code_ERR_INPUT_FAILS_PRECONDITION, violations...)
			}
			if inconsistent{{BaseType}} {
				errors.
					SetCode(code.Code_INVALID_ARGUMENT).
					AddGenericErrorCode(mc.Code_ERR_INVALID_TRANSACTION)
			}
			resp.Error = errors.Build()

			return resp, svc.RPCError(resp.Error.Code, resp)
		// database errors
		case *mysql.MySQLError:

			switch int(e.Number) {

			// double uuid somehow
			case mysqlerr.ER_DUP_ENTRY:
				errors.
					SetCode(code.Code_ALREADY_EXISTS).
					AddBadRequestFieldViolations(mc.Code_ERR_DUPLICATE_ENTRY, &errdetails.BadRequest_FieldViolation{
						Field:       "id",
						Description: TF(ctx)(mc.Code_ERR_DUPLICATE_ENTRY.String()),
					})

			}

		default:
			{{FirstLetter}}.Logger.Errorf("%T", err)
		}
		resp.Error = errors.Build()
		return resp, svc.RPCError(resp.Error.Code, resp)
	}

	resp.Data = {{FirstLetter}}.ToRPC({{baseTypeLower}})

	resp.Success = true

	return resp, nil

}

// Update -- (PUT) request to change some of the fields of a {{baseTypeLower}}. Returns the updated {{BaseType}} object
func ({{FirstLetter}} *{{BaseType}}) Update(ctx context.Context, req *rpc.{{BaseType}}UpdateRequest) (*rpc.{{BaseType}}UpdateResponse, error) {

	resp := &rpc.{{BaseType}}UpdateResponse{
		Success: false,
	}
	// same as create, don't use req until validateMessage has been called or you risk segfaults
	if errors := {{FirstLetter}}.validateMessage(ctx, req); errors != nil {
		resp.Error = errors.Build()
		return resp, svc.RPCError(resp.Error.Code, resp)
	}
	if !{{FirstLetter}}.verifyOwner(ctx, req.{{BaseType}}.PropertyId) {
		errors := {{FirstLetter}}.Errors(ctx, code.Code_INTERNAL)
		errors.
			SetCode(code.Code_NOT_FOUND).
			AddGenericErrorCode(mc.Code_ERR_NO_PERMISSIONS)
		resp.Error = errors.Build()
		return resp, svc.RPCError(resp.Error.Code, resp)
	}
	{{baseTypeLower}}Repo := repo.New{{BaseType}}(ctx)
	new{{BaseType}} := {{FirstLetter}}.ToModel(req.{{BaseType}}, req.{{BaseType}}.Id, auth.GetUserIDFromContext(ctx))

	// make repo call here
	new{{BaseType}}, err := {{baseTypeLower}}Repo.Update(new{{BaseType}})
	// error handling
	if err != nil {
		{{FirstLetter}}.Logger.Debug(err)
		errors := {{FirstLetter}}.Errors(ctx, code.Code_INTERNAL)
		switch err.(type) {
		// handle validation errors differently from database errors
		case govalidator.Errors:
			errors.SetCode(code.Code_INVALID_ARGUMENT)
			violations := []*errdetails.BadRequest_FieldViolation{}
			for k := range govalidator.ErrorsByField(err) {
				violations = append(violations, &errdetails.BadRequest_FieldViolation{
					Field:       k,
					Description: TF(ctx)(mc.Code_ERR_INPUT_FAILS_PRECONDITION.String()),
				})
			}

			errors.AddBadRequestFieldViolations(mc.Code_ERR_INPUT_FAILS_PRECONDITION, violations...)
			resp.Error = errors.Build()

			return resp, svc.RPCError(resp.Error.Code, resp)
		// database errors -- cannot find {{baseTypeLower}} to update
		case *mysql.MySQLError:
			errors.
				SetCode(code.Code_NOT_FOUND).
				AddGenericErrorCode(mc.Code_ERR_TRANSACTION_NOT_FOUND)
		case repo.ErrorNotFound:
			errors.
				SetCode(code.Code_NOT_FOUND).
				AddGenericErrorCode(mc.Code_ERR_TRANSACTION_NOT_FOUND)
		default:
			{{FirstLetter}}.Logger.Errorf("%T", err)

		}
		resp.Error = errors.Build()
		return resp, svc.RPCError(resp.Error.Code, resp)
	}
	resp.Success = true
	resp.Data = {{FirstLetter}}.ToRPC(new{{BaseType}})

	return resp, nil

}

// Delete -- DELETE request to a {{baseTypeLower}} to remove it from the database. Returns a boolean signalling success
func ({{FirstLetter}} *{{BaseType}}) Delete(ctx context.Context, req *rpc.{{BaseType}}DeleteRequest) (*rpc.{{BaseType}}DeleteResponse, error) {
	{{baseTypeLower}}Repo := repo.New{{BaseType}}(ctx)
	resp := &rpc.{{BaseType}}DeleteResponse{
		Success: false,
	}
	// {{FirstLetter}}.Logger.Debugln(req.Id)
	res, err := {{baseTypeLower}}Repo.Delete({{baseTypeLower}}Repo.MakeLookup(req.Id, "", ""))
	if err != nil || !res { // the only possible error here is that the lookup fails
		errors := {{FirstLetter}}.Errors(ctx, code.Code_INTERNAL)
		errors.
			SetCode(code.Code_NOT_FOUND).
			AddGenericErrorCode(mc.Code_ERR_TRANSACTION_NOT_FOUND)
		resp.Error = errors.Build()

		return resp, svc.RPCError(resp.Error.Code, resp)
	}
	resp.Success = res
	return resp, nil

}

// ToRPC goes from a *model.{{BaseType}} object to a *rpc.{{BaseType}} object
func ({{FirstLetter}} *{{BaseType}}) ToRPC(in *model.{{BaseType}}) *rpc.{{BaseType}} {
	return &rpc.{{BaseType}}{
		Id: in.ID, // TOOD: Add other fields
	}
}

// ToModel takes in a {{BaseType}}Address and id and spits out a *model.{{BaseType}} object
func ({{FirstLetter}} *{{BaseType}}) ToModel(in *rpc.{{BaseType}}, id, ownerID string) *model.{{BaseType}} {
	return &model.{{BaseType}}{
		ID: id, // TODO: Add other fields
	}
}

type {{baseTypeLower}}Request interface {
	Get{{BaseType}}() *rpc.{{BaseType}}
}

func ({{FirstLetter}} *{{BaseType}}) genericBadRequest(ctx context.Context, fields ...string) *svc.Errors {
	errors := {{FirstLetter}}.Errors(ctx, code.Code_INTERNAL)
	errors.SetCode(code.Code_INVALID_ARGUMENT)

	violations := []*errdetails.BadRequest_FieldViolation{}
	for _, field := range fields {
		violations = append(violations, &errdetails.BadRequest_FieldViolation{
			Field:       field,
			Description: TF(ctx)(mc.Code_ERR_BAD_INPUT.String()),
		})
	}

	errors.AddBadRequestFieldViolations(mc.Code_ERR_BAD_INPUT, violations...)
	return errors
}

func ({{FirstLetter}} *{{BaseType}}) validateMessage(ctx context.Context, message proto.Message) *svc.Errors {

	switch t := message.(type) {
	case {{baseTypeLower}}Request:
		{{FirstLetter}}.Logger.Debug(message)
		if t.Get{{BaseType}}() == nil {
			return {{FirstLetter}}.genericBadRequest(ctx, "{{baseTypeLower}}")
		}
		return {{FirstLetter}}.validateMessage(ctx, t.Get{{BaseType}}())
	}
	return nil
}

func ({{FirstLetter}} *{{BaseType}})  verifyOwner(ctx context.Context, propertyID string) bool {
	propRepo := repo.NewIncome(ctx)
	foundProp, err := propRepo.Get(propRepo.MakeLookup(propertyID, auth.GetUserIDFromContext(ctx)))
	if err != nil || foundProp.ID == "" { // the only possible error here is that the lookup fails or returns an empty Property object, whereby the ID will be the empty string
		return false
	}

	return true

}
