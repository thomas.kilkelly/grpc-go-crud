package repo

import (
	"github.com/asaskevich/govalidator"
	"gitlab.com/lodg/go-util/db"
	"gitlab.com/lodg/go-util/logging"
	"gitlab.com/lodg/go-util/proto/api"
	"gitlab.com/lodg/go-util/validators"

	"gitlab.com/lodg/go-util/cursor"
	"gitlab.com/lodg/svc-properties/repo/model"
	"golang.org/x/net/context"
)

const (
	// Err{{BaseType}}NotFound error string
	Err{{BaseType}}NotFound = "{{baseTypeLower}} not found (ID lookup)"
	// ErrInconsistent{{BaseType}} fake error field name
	ErrInconsistent{{BaseType}} = "inconsistent_{{baseTypeLower}}"
)

// {{BaseType}}Repo repository
type {{BaseType}}Repo struct {
	ctx    context.Context
	logger *logging.Logger
}

// New{{BaseType}} get the {{baseTypeLower}} repo
func New{{BaseType}}(ctx context.Context, driver ...*db.DB) *{{BaseType}}Repo {
	return &{{BaseType}}Repo{
		ctx:    ctx,
		logger: logging.NewWithContext(ctx, "repo.{{baseTypeLower}}"),
	}
}

// Get a specific {{baseTypeLower}} by its id from the database -- basically a wrapper for Get{{BaseType}}ByLookup
// but there may be reason for more functionality to be introduced later
func ({{FirstLetter}} *{{BaseType}}Repo) Get(lookup *model.{{BaseType}}) (*model.{{BaseType}}, error) {
	return {{FirstLetter}}.Get{{BaseType}}ByLookup(lookup)
}

// List all {{baseTypeLower}}s with the specific propertyID given a cursor, limit and offset.
// If a cursor is provided:
//  -   the limit provided is used (the cursor's limit is used only if the provided limit is 0), which allows you to specify a larger page size without redoing your cursor enquiry
// 	-   the offset is added to the value in the cursor, allowing you to skip pages if you want (set the offset to 0 otherwise)
func ({{FirstLetter}} *{{BaseType}}Repo) List(filter *model.{{BaseType}}Filter, cursorBase64 string, limit uint32, offset uint32) (*model.{{BaseType}}Collection, error) {

	var currentCursor *cursor.Cursor
	currentCursor, realLimit, realOffset, err := cursor.HandleCursor(cursorBase64, limit, offset)
	if err != nil {
		return nil, err
	}
	{{baseTypeLower}}s, err := {{FirstLetter}}.Find{{BaseType}}sByFilter(filter, 1+realLimit, realOffset)
	if err != nil {
		return nil, err
	}

	return model.New{{BaseType}}Collection(currentCursor, {{baseTypeLower}}s), nil
}

// Create a new {{baseTypeLower}} in the database
func ({{FirstLetter}} *{{BaseType}}Repo) Create(tx *model.{{BaseType}}) (*model.{{BaseType}}, error) {
	tx.ID = db.GenerateID()
	_, err := govalidator.ValidateStruct(tx)
	if err != nil {
		{{FirstLetter}}.logger.Errorf("invalid {{baseTypeLower}} with error: %s", err)

		return nil, validators.Investigate(err, api.Code_ERR_INCONSISTENT_TRANSACTION)
	}
	// make the database call to insert
	session := DBReadWrite().NewSession()
	defer session.Close()
	_, err = session.InsertOne(tx)
	if err != nil {
		{{FirstLetter}}.logger.Errorf("failed to create {{baseTypeLower}} with error: %s", err)
		return nil, err
	}
	return tx, nil
}

// Update updates the {{baseTypeLower}} with the values in the argument tx
func ({{FirstLetter}} *{{BaseType}}Repo) Update(tx *model.{{BaseType}}) (*model.{{BaseType}}, error) {
	var err error
	_, err = govalidator.ValidateStruct(tx)
	if err != nil {
		{{FirstLetter}}.logger.Errorf("invalid {{baseTypeLower}} to update with error: %s", err)
		return nil, err
	}

	// check it exist
	_, err = {{FirstLetter}}.Get{{BaseType}}ByLookup({{FirstLetter}}.MakeLookup(tx.ID, tx.OwnerID, tx.PropertyID))
	if err != nil {
		return nil, err
	}
	// make the database call to update
	session := DBReadWrite().NewSession()
	defer session.Close()
	_, err = session.ID(tx.ID).AllCols().Update(tx) // remove id from tx and do primary key lookup before this
	if err != nil {
		return nil, err
	}
	session.Commit()
	return tx, err
}

// Delete doesn't actually delete the {{baseTypeLower}}, it just sets the isDeleted flag in the database to true
func ({{FirstLetter}} *{{BaseType}}Repo) Delete(lookup *model.{{BaseType}}) (bool, error) {
	session := DBReadWrite().NewSession()
	defer session.Close()
	numDel, err := session.Delete(lookup)
	if numDel == 0 || err != nil {
		return false, err
	}
	return true, nil
}

// Get{{BaseType}}ByLookup gets NONDELETED {{baseTypeLower}} from the database based on matching its ID
func ({{FirstLetter}} *{{BaseType}}Repo) Get{{BaseType}}ByLookup(lookup *model.{{BaseType}}) (*model.{{BaseType}}, error) {
	session := DBReadWrite().NewSession()
	defer session.Close()
	isFound, err := session.Get(lookup)
	if err != nil {
		return nil, err
	} else if !isFound { // returns {{baseTypeLower}} not found error if the {{baseTypeLower}} is marked as 'deleted'
		return nil, NewErrorNotFound(Err{{BaseType}}NotFound)
	}
	return lookup, nil
}

// Find{{BaseType}}sByLookup is same as above but returns a bunch
// Passing in a limit with a negative number makes the lookup not use a Limit at all
func ({{FirstLetter}} *{{BaseType}}Repo) Find{{BaseType}}sByLookup(lookup *model.{{BaseType}}, limit uint32, offset uint32) ([]*model.{{BaseType}}, error) {
	var {{baseTypeLower}}s []*model.{{BaseType}}
	session := DBReadWrite().NewSession()
	defer session.Close()

	err := model.New{{BaseType}}Query(session).
		Session.Limit(int(limit), int(offset)).Find(&{{baseTypeLower}}s, lookup)

	if err != nil {
		return nil, err
	}
	return {{baseTypeLower}}s, err
}

// Find{{BaseType}}sByFilter get {{baseTypeLower}}s give a filter
func ({{FirstLetter}} *{{BaseType}}Repo) Find{{BaseType}}sByFilter(filter *model.{{BaseType}}Filter, limit uint32, offset uint32) ([]*model.{{BaseType}}, error) {
	var {{baseTypeLower}}s []*model.{{BaseType}}
	session := DBReadWrite().NewSession()
	defer session.Close()
	err := model.New{{BaseType}}Query(session).
		WithFilter(filter).
		Session.Limit(int(limit), int(offset)).Find(&{{baseTypeLower}}s)

	if err != nil {
		return nil, err
	}
	return {{baseTypeLower}}s, err
}

// GetAll{{BaseType}}ByLookup gets all (including 'deleted') {{baseTypeLower}}s from the database based on matching its ID
func ({{FirstLetter}} *{{BaseType}}Repo) GetAll{{BaseType}}ByLookup(lookup *model.{{BaseType}}) (*model.{{BaseType}}, error) {
	session := DBReadWrite().NewSession()
	defer session.Close()
	isFound, err := session.Unscoped().Get(lookup)
	if err != nil {
		return nil, err
	} else if !isFound { // returns {{baseTypeLower}} not found error if the {{baseTypeLower}} is marked as 'deleted'
		return nil, NewErrorNotFound(Err{{BaseType}}NotFound)
	}
	return lookup, nil
}

// MakeLookup makes a model.{{BaseType}} object with required values for lookup
func ({{FirstLetter}} *{{BaseType}}Repo) MakeLookup(id string, ownerID string, propertyID string) *model.{{BaseType}} {
	return &model.{{BaseType}}{
		ID:         id,
		OwnerID:    ownerID,
		PropertyID: propertyID,
	}
}
