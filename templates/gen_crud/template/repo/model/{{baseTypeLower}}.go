package model

import (
	"gitlab.com/lodg/go-util/cursor"

	"gitlab.com/lodg/go-util/models"
)



// {{BaseType}}Filter is a filter object
type {{BaseType}}Filter struct {
	ID           string
}



// {{BaseType}} type
type {{BaseType}} struct {
	ID string `valid:"uuidv4_base64,runelength(1|255)" xorm:"pk 'id'" json:"id"`// TODO: insert tags as necessary

	// meta_whatever later on
	DeletedAt time.Time `valid:"-" xorm:"deleted 'deleted_at'" json:"deleted_at"`
	CreatedAt time.Time `valid:"-" xorm:"created 'created_at'" json:"created_at"`
	UpdatedAt time.Time `valid:"-" xorm:"updated 'updated_at'" json:"updated_at"`
}

// TruncateTime chops off decimal places on time.Time objects inside our model.Transaction objects
func (m *{{BaseType}}) TruncateTime() {
	m.DeletedAt = m.DeletedAt.Truncate(time.Second)
	m.CreatedAt = m.CreatedAt.Truncate(time.Second) // need to truncate off microseconds because xorm likes to pretend timestamp objects are datetime objects
	m.UpdatedAt = m.UpdatedAt.Truncate(time.Second)
}

// {{BaseType}}Collection consists of an array of {{BaseType}} with information about it in the Collection object
type {{BaseType}}Collection struct {
	*models.Collection
	items []*{{BaseType}}
}

// New{{BaseType}}Collection makes a new {{BaseType}}colection obejct
func New{{BaseType}}Collection(current *cursor.Cursor, items []*{{BaseType}}) *{{BaseType}}Collection {
	return &{{BaseType}}Collection{
		&models.Collection{
			Count:   uint32(len(items)),
			Current: current,
		}, items,
	}
}

// GetItems gets the items item from the {{baseTypeLower}}collection
func (t *{{BaseType}}Collection) GetItems() []*{{BaseType}} {
	if t.Count > t.Current.Limit {
		return t.items[:t.Current.Limit]
	}
	return t.items
}

func init() {
}

// // IsConsistent is the consistency checker for a ConsistencyChecker interface
// func (m *{{BaseType}}) IsConsistent() bool {
// 	// TODO: fill this out if needed
// }

// func (m *{{BaseType}}) toCSV() []string {
// 	return []string{ // TODO: fill these out if needed
// 	}
// }
// func (m *{{BaseType}}) toCSVHeaders() []string {
// 	return []string{ // TODO: fill these out if needed
// 	}
// }
