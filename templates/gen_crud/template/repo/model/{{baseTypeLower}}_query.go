package model

import (
	"fmt"
	"time"

	"github.com/go-xorm/builder"
	"github.com/go-xorm/xorm"
	"gitlab.com/lodg/go-util/fn"
)

// {{BaseType}}FieldNames is a struct that holds the field names of the model.{{BaseType}} object
type {{BaseType}}FieldNames struct {
	ID          string
	PropertyID  string
	RecurringID string
	OwnerID     string
	DeletedAt   string
	CreatedAt   string
	UpdatedAt   string
}

var {{FirstLetter}}Fields = {{BaseType}}FieldNames{
	ID:          "id",
	PropertyID:  "property_id",
	RecurringID: "recurring_id",
	OwnerID:     "owner_id",

	DeletedAt: "deleted_at",
	CreatedAt: "created_at",
	UpdatedAt: "updated_at",
}

// {{BaseType}}Field returns the database column name of a field of a model.{{BaseType}} object
func {{BaseType}}Field() {{BaseType}}FieldNames {
	return {{FirstLetter}}Fields
}

// {{BaseType}}Query type to provide an easier time making Where queries to our db
type {{BaseType}}Query struct {
	Session *xorm.Session
}

// New{{BaseType}}Query makes a new {{BaseType}}Query
func New{{BaseType}}Query(session *xorm.Session) *{{BaseType}}Query {
	return &{{BaseType}}Query{
		Session: session,
	}
}

// Where is an interface extension
func ({{FirstLetter}} *{{BaseType}}Query) Where(stmt interface{}, args ...interface{}) *{{BaseType}}Query {
	{{FirstLetter}}.Session = {{FirstLetter}}.Session.Where(stmt, args...)
	return {{FirstLetter}}
}

// Eq is for comparing equality
func ({{FirstLetter}} *{{BaseType}}Query) Eq(fld string, val interface{}) *{{BaseType}}Query {
	{{FirstLetter}}.Where(builder.Eq{fld: val})
	return {{FirstLetter}}
}

// Lt is for less than
func ({{FirstLetter}} *{{BaseType}}Query) Lt(fld string, val interface{}) *{{BaseType}}Query {
	{{FirstLetter}}.Where(builder.Lt{fld: val})
	return {{FirstLetter}}
}

// Gt is for more than
func ({{FirstLetter}} *{{BaseType}}Query) Gt(fld string, val interface{}) *{{BaseType}}Query {
	{{FirstLetter}}.Where(builder.Gt{fld: val})
	return {{FirstLetter}}
}

// Neq is for not equal
func ({{FirstLetter}} *{{BaseType}}Query) Neq(fld string, val interface{}) *{{BaseType}}Query {
	{{FirstLetter}}.Where(builder.Neq{fld: val})
	return {{FirstLetter}}
}

// Between for a combination of gt and lt I guess
func ({{FirstLetter}} *{{BaseType}}Query) Between(fld string, lessVal interface{}, moreVal interface{}) *{{BaseType}}Query {
	{{FirstLetter}}.Where(builder.Between{
		Col:     fld,
		LessVal: lessVal,
		MoreVal: moreVal,
	})
	return {{FirstLetter}}
}

// In is for things inside (note you can also pass arrays to Eq and Neq which is what you'd rather have honestly)
func ({{FirstLetter}} *{{BaseType}}Query) In(fld string, vals ...interface{}) *{{BaseType}}Query {
	{{FirstLetter}}.Where(builder.In(fld, vals))
	return {{FirstLetter}}
}

// NotIn is for things inside (note you can also pass arrays to Eq and Neq which is what you'd rather have honestly)
func ({{FirstLetter}} *{{BaseType}}Query) NotIn(fld string, vals ...interface{}) *{{BaseType}}Query {
	{{FirstLetter}}.Where(builder.NotIn(fld, vals))
	return {{FirstLetter}}
}

// Lte is for more than
func ({{FirstLetter}} *{{BaseType}}Query) Lte(fld string, val interface{}) *{{BaseType}}Query {
	{{FirstLetter}}.Where(builder.Lte{fld: val})
	return {{FirstLetter}}
}

// Gte is for more than
func ({{FirstLetter}} *{{BaseType}}Query) Gte(fld string, val interface{}) *{{BaseType}}Query {
	{{FirstLetter}}.Where(builder.Gte{fld: val})
	return {{FirstLetter}}
}

// WithID gets ID from our {{BaseType}} object
func ({{FirstLetter}} *{{BaseType}}Query) WithID(id string) *{{BaseType}}Query {
	return {{FirstLetter}}.Eq({{FirstLetter}}Fields.ID, id)
}

// WithPropertyID gets PropertyID from our {{BaseType}} object
func ({{FirstLetter}} *{{BaseType}}Query) WithPropertyID(propertyID string) *{{BaseType}}Query {
	return {{FirstLetter}}.Eq({{FirstLetter}}Fields.PropertyID, propertyID)
}

// WithRecurringID gets RecurringID from our {{BaseType}} object
func ({{FirstLetter}} *{{BaseType}}Query) WithRecurringID(recurringID string) *{{BaseType}}Query {
	return {{FirstLetter}}.Eq({{FirstLetter}}Fields.RecurringID, recurringID)
}

// WithOwnerID gets OwnerID from our {{BaseType}} object
func ({{FirstLetter}} *{{BaseType}}Query) WithOwnerID(ownerID string) *{{BaseType}}Query {
	return {{FirstLetter}}.Eq({{FirstLetter}}Fields.OwnerID, ownerID)
}

// WithDeletedAt gets DeletedAt from our {{BaseType}} object
func ({{FirstLetter}} *{{BaseType}}Query) WithDeletedAt(deletedAt time.Time) *{{BaseType}}Query {
	return {{FirstLetter}}.Eq({{FirstLetter}}Fields.DeletedAt, deletedAt)
}

// WithCreatedAt gets CreatedAt from our {{BaseType}} object
func ({{FirstLetter}} *{{BaseType}}Query) WithCreatedAt(createdAt time.Time) *{{BaseType}}Query {
	return {{FirstLetter}}.Eq({{FirstLetter}}Fields.CreatedAt, createdAt)
}

// WithUpdatedAt gets UpdatedAt from our {{BaseType}} object
func ({{FirstLetter}} *{{BaseType}}Query) WithUpdatedAt(updatedAt time.Time) *{{BaseType}}Query {
	return {{FirstLetter}}.Eq({{FirstLetter}}Fields.UpdatedAt, updatedAt)
}

// SQL ddd
func ({{FirstLetter}} *{{BaseType}}Query) SQL(query string, args ...interface{}) *{{BaseType}}Query {
	{{FirstLetter}}.Session = {{FirstLetter}}.Session.SQL(fmt.Sprintf(query, "{{BaseType}}"), args...)
	return {{FirstLetter}}
}

// WithFilter create a query with the given filter object
func ({{FirstLetter}} *{{BaseType}}Query) WithFilter(filter *{{BaseType}}Filter) *{{BaseType}}Query {

	if filter.ID != "" {
		{{FirstLetter}}.WithID(filter.ID)
	}

	if filter.OwnerID != "" {
		{{FirstLetter}}.WithOwnerID(filter.OwnerID)
	}

	if filter.RecurringIDs != nil && len(filter.RecurringIDs) > 0 {
		{{FirstLetter}}.Where(builder.In({{FirstLetter}}Fields.RecurringID, fn.ToInterfaces(filter.RecurringIDs)))
	}

	if filter.PropertyIDs != nil && len(filter.PropertyIDs) > 0 {
		{{FirstLetter}}.Where(builder.In({{FirstLetter}}Fields.PropertyID, fn.ToInterfaces(filter.PropertyIDs)))
	}

	return {{FirstLetter}}

}
